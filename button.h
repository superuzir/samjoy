#ifndef BUTTON_H
#define BUTTON_H

#include <QLabel>

class Button : public QLabel {
    Q_OBJECT

public:
    explicit Button(QWidget *parent = 0, int n = -1, QString pon = 0, QString poff = 0);
    void paintEvent(QPaintEvent *event);
    void set(bool p);
    bool get(){ return pressed; }

private:
    int number;
    bool pressed;
    QString path_on;
    QString path_off;
};

#endif // BUTTON_H
