#include <QPainter>
#include <QDebug>

#include "button.h"

Button::Button(QWidget *parent, int n, QString pon, QString poff) :
    QLabel(parent)
{
    number = n;
    pressed = false;
    path_on = pon;
    path_off = poff;

    QPixmap circle(pon);
    setMinimumSize(circle.size());
}

void Button::paintEvent(QPaintEvent * event){
    QLabel::paintEvent(event);
    QPainter painter(this);

    int w = width();
    int h = height();

    QString circle_path;
    if(pressed){
        circle_path = path_on;
    } else {
        circle_path = path_off;
    }

    QPixmap circle(circle_path);

    int r = circle.width();

    painter.drawPixmap(w/2 - r/2, h/2 - r/2, r, r, circle);

    painter.setPen(Qt::black);
    if(number != -1) painter.drawText(QRect(w/2 - r/2, h/2 - r/2, r, r), Qt::AlignCenter, QString::number(number));
}


void Button::set(bool p){
    pressed = p;
    repaint();
}
