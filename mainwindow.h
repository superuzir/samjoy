#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProgressBar>

#include "feeder.h"
#include "server.h"
#include "button.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Feeder *feeder;
    Server *server;
    QVector<Button *> btn_vec;
    QVector<QProgressBar *> axis_vec;
    Button *conn_status;

    void setBtn(int id, bool value);

public slots:
    void buttonPressed(int id);
    void buttonReleased(int id);
    void axisChanged(int id, int value);
    void connected(bool);
};

#endif // MAINWINDOW_H
