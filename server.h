#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QTimer>

#include "JoyPacket.h"

class Server : public QTcpServer
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);
    ~Server();


private:
    QTcpSocket server_socket;
    QTimer *alive_timer;
    bool is_alive;

    QHostAddress pc_ip;

public slots:

      void tcpReady();

      void tcpError( QAbstractSocket::SocketError error );

      bool startListen(int port_no);

      void checkAlive();

protected:
    void incomingConnection( int descriptor );

signals:
    void packet(JoyPacket packet);
    void alive(bool is_alive);

};

#endif // SERVER_H
