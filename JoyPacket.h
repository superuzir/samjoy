#ifndef JOYPACKET_H
#define JOYPACKET_H

typedef struct {
    quint32 type;
    quint32 id;
    quint32 value;
} JoyPacket;

#endif // JOYPACKET_H
