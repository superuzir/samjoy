#ifndef FEEDER_H
#define FEEDER_H

#include <QObject>
#include <QTimer>
#include <QTextEdit>

#include "JoyPacket.h"

class Feeder : public QObject
{
    Q_OBJECT
public:
    explicit Feeder(QObject *parent = 0, int btn_cnt = 0);

private:
    uint dev_id;
    uint debug_cnt;
    QList<uint> to_press;
    QList<uint> pressed;
    QList<JoyPacket> active_axis;
    QTimer axis_tmr;
    QTimer heartbeat;
    QVector< QList<bool> *> event_queues;

public slots:
    void feed(JoyPacket p);
    void setDevId(int d);
    void refreshAxis();
    void tick();

signals:
    void buttonPressed(int id);
    void buttonReleased(int id);
    void axisChanged(int id, int value);
};

#endif // FEEDER_H
