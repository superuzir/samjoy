#include <QUdpSocket>
#include <QNetworkInterface>
#include "server.h"



Server::Server(QObject *parent) : QTcpServer(parent){
    connect( &server_socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(tcpError(QAbstractSocket::SocketError)) );
    connect( &server_socket, SIGNAL(readyRead()),
             this, SLOT(tcpReady()) );

    server_socket.setSocketOption(QAbstractSocket::KeepAliveOption, true );

    // Between ticks we should have any packet, otherwise connection is down
    alive_timer = new QTimer(this);
    connect(alive_timer, SIGNAL(timeout()), this, SLOT(checkAlive()));
    alive_timer->start(3000);

    is_alive = false;

    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol &&
                address != QHostAddress(QHostAddress::LocalHost)){
//                qDebug() << address.toString();
            pc_ip = address;
        }
    }
}



Server::~Server(){
    server_socket.disconnectFromHost();
    server_socket.waitForDisconnected();
}



void Server::tcpReady(){
    QByteArray array = server_socket.read( server_socket.bytesAvailable() );

    if(array.size() % sizeof(JoyPacket) == 0){
        int count = array.size() / sizeof(JoyPacket);
        JoyPacket p[count];
        for(int i = 0; i < count; i++){
            memcpy(&p[i], array.constData() + i * sizeof(JoyPacket), sizeof(JoyPacket));
            emit packet(p[i]);
            emit alive(true);
            is_alive = true;
        }
    } else {
        qDebug() << "hmmmm" << array.size();
    }

}



void Server::tcpError(QAbstractSocket::SocketError error){
    (void) error;
    qDebug() << server_socket.errorString();
}



bool Server::startListen(int port_no){
    if( !this->listen( QHostAddress::Any, port_no ) ){
        qDebug() << "Error! Cannot listen to port" << port_no;

        return false;
    } else {
        qDebug() << "Listening to port" << port_no;
        return true;
    }
}


void Server::checkAlive(){
    if(is_alive){
        emit alive(true);
        is_alive = false;
    } else {
        emit alive(false);
    }


    QUdpSocket bc_socket;

    bc_socket.connectToHost(QHostAddress(QHostAddress::Broadcast), 22001, QIODevice::ReadWrite);

    QByteArray datagram = pc_ip.toString().toUtf8();

    bc_socket.write(datagram);
}



void Server::incomingConnection(int descriptor){
    if( !server_socket.setSocketDescriptor( descriptor ) )
    {
        qDebug() << "Error! Socket error!";
        return;
    }
    qDebug() << "New connection";
}

