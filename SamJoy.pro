#-------------------------------------------------
#
# Project created by QtCreator 2015-11-04T18:30:57
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SamJoy
TEMPLATE = app


SOURCES += main.cpp\
    feeder.cpp \
    server.cpp \
    button.cpp \
    mainwindow.cpp

HEADERS  += \
    feeder.h \
    qdebugstream.h \
    server.h \
    JoyPacket.h \
    button.h \
    mainwindow.h

FORMS    += \
    mainwindow.ui

INCLUDEPATH += vjoy\\inc

LIBS += -L..\\SamJoy\\vjoy\\lib \
        -lvJoyInterface \
        -luser32


QMAKE_CXXFLAGS_WARN_ON += -Wno-unknown-pragmas # Disable VJoy warnings

RESOURCES += \
    resources.qrc
