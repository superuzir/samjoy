#include <QDebug>


#include "stdafx.h"
#include "public.h"
#include "vjoyinterface.h"
#include "feeder.h"

Feeder::Feeder(QObject *parent, int btn_cnt) : QObject(parent){
    dev_id = 1;

    // Connect joystick
    // Get the driver attributes (Vendor ID, Product ID, Version Number)
    if(!vJoyEnabled()){
        qDebug() << "Function vJoyEnabled Failed - make sure that vJoy is installed and enabled";
        return;
    } else {
        qDebug() << "Vendor: " << static_cast<wchar_t *>(GetvJoyManufacturerString());
        qDebug() << "Product: " << static_cast<wchar_t *>(GetvJoyProductString());
        qDebug() << "Version:" <<  static_cast<wchar_t *>(GetvJoySerialNumberString());
    }


    // Acquire the vJoy device
    if (!AcquireVJD(dev_id)){
        qDebug() << "Failed to acquire vJoy device number" << dev_id;
        return;
    } else {
        qDebug() << "Acquired device number" << dev_id << "OK";
    }

    for(int i = 0; i < btn_cnt + 1; i++){
        event_queues.append(new QList<bool>);
    }

    connect(&axis_tmr, SIGNAL(timeout()), this, SLOT(refreshAxis()));
    connect(&heartbeat, SIGNAL(timeout()), this, SLOT(tick()));

    axis_tmr.start(5000);
    heartbeat.start(50);
}


void Feeder::tick(){
    for(int i = 0; i < event_queues.size(); i++){
        QList<bool> *event_queue = event_queues.at(i);
        if(!event_queue->isEmpty()){
            bool event = event_queue->takeFirst();
            SetBtn(event, dev_id, i);
            if(event)
                emit buttonPressed(i);
            else
                emit buttonReleased(i);
        }
    }
}


void Feeder::feed(JoyPacket p){
    if((int)p.id > event_queues.size())
        return;

    switch(p.type){

        case 1:
            if(p.value == FALSE) break;
            qDebug() << "Button" << p.id << "clicked";
            event_queues[p.id]->append(true);
            event_queues[p.id]->append(false);
            break;

        case 2:
            qDebug() << "Button" << p.id << (p.value ? "pushed" : "released");
            event_queues[p.id]->append(p.value);
            break;

        case 10:
        {
            qDebug() << "Axis" << p.id << "value =" << p.value;
            SetAxis(p.value, dev_id, HID_USAGE_Z + p.id);
            emit axisChanged(p.id, p.value);
            bool found = false;
            for(int i = 0; i < active_axis.size(); i++){
                if(active_axis.at(i).id == p.id){
                    found = true;
                    active_axis[i].value = p.value;
                }
            }
            if(!found){
                active_axis.append(p);
            }
            break;
        }
        default:
            break;
    }
}


void Feeder::setDevId(int d){
    dev_id = d;

    // Acquire the vJoy device
    if (!AcquireVJD(dev_id)){
        qDebug() << "Failed to acquire vJoy device number" << dev_id;
        return;
    } else {
        qDebug() << "Acquired device number" << dev_id << "OK";
    }
}



void Feeder::refreshAxis(void){
    if(active_axis.empty()) return;

    static int i = 0;
    i %= active_axis.size();

    SetAxis(0, dev_id, HID_USAGE_Z + active_axis.at(i).id);
    SetAxis(active_axis.at(i).value, dev_id, HID_USAGE_Z + active_axis.at(i).id);
//    qDebug() << active_axis.at(i).id << active_axis.at(i).value;

    i++;
}
