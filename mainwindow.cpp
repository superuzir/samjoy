#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qdebugstream.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    new Q_DebugStream(std::cout, ui->log); //Redirect Console output to QTextEdit
    Q_DebugStream::registerQDebugMessageHandler(); //Redirect qDebug() output to QTextEdit

    int btn_cnt = 0;
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            Button *btn = new Button(this,
                                     8 * i + j + 1,
                                     ":/icons/circle_red.png",
                                     ":/icons/circle_grey.png");
            ui->grid_btn->addWidget(btn, i, j);
            btn_vec.append(btn);
            btn_cnt++;
        }
    }

    feeder = new Feeder(this, btn_cnt);

    for(int i = 0; i < 8; i++){
        QProgressBar *axis = new QProgressBar(this);
        axis->setMaximum(32767);

        ui->grid_axis->addWidget(axis);
        axis_vec.append(axis);
    }


    conn_status = new Button(this,
                             -1,
                             ":/icons/circle_green.png",
                             ":/icons/circle_grey.png");

    ui->grid_conn->addWidget(conn_status, 0, 1);

    server = new Server(this);
    server->startListen(22000);

    connect(server, SIGNAL(packet(JoyPacket)), feeder, SLOT(feed(JoyPacket)));
    connect(server, SIGNAL(alive(bool)), this, SLOT(connected(bool)));
    connect(ui->dev_id, SIGNAL(valueChanged(int)), feeder, SLOT(setDevId(int)));
    connect(feeder, SIGNAL(buttonPressed(int)), this, SLOT(buttonPressed(int)));
    connect(feeder, SIGNAL(buttonReleased(int)), this, SLOT(buttonReleased(int)));
    connect(feeder, SIGNAL(axisChanged(int, int)), this, SLOT(axisChanged(int, int)));
}

MainWindow::~MainWindow()
{
    delete server;
    delete feeder;
    delete ui;
}

void MainWindow::setBtn(int id, bool value){
    if(id < 0) return;
    if(id >= btn_vec.size()) return;

    btn_vec.at(id - 1)->set(value);
}


void MainWindow::buttonPressed(int id){
    setBtn(id, true);
}

void MainWindow::buttonReleased(int id){
    setBtn(id, false);
}

void MainWindow::axisChanged(int id, int value){
    if(id < 1) return;
    if(id > axis_vec.size()) return;

    axis_vec.at(id - 1)->setValue(value);
}

void MainWindow::connected(bool online){
    conn_status->set(online);
}
